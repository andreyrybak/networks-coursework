#ifndef TCP_SOCKET_H
#define TCP_SOCKET_H
#include <cstdint>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string>

int tcp_socket(addrinfo * &, uint16_t, const std::string &);

void send_sure(int, char *, size_t, const std::string &);

#endif

