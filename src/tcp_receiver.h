#ifndef TCP_RECEIVER_H
#define TCP_RECEIVER_H

struct ips_receiver
{
    ips_receiver();
    void operator()(const std::vector<uint32_t> &);
private:
    int sock;
    struct sockaddr_in sock_in;
    socklen_t si_len;
    struct addrinfo *servinfo;
    int epollfd;
    fd_set fds;
    int maxfd;

    const std::string name = "ips_receiver";
};

#endif
