#ifndef UDP_H
#define UDP_H

#include <string>

void make_udp_socket(int &, struct sockaddr_in &, uint16_t, std::string, bool is_receiver = false);

void bind_device(int, const std::string &, const std::string &);

void set_broadcast(int, const std::string &);

void set_timeout(int, const time_t, const suseconds_t, const std::string &);
#endif
