#ifndef RECEIVER_H
#define RECEIVER_H
#include <cstdint>
#include <netinet/in.h>


#include <vector>
#include <string>

struct receiver
{
    receiver(const uint16_t);
    void cycle();
    ~receiver();

    std::string get_phrase(size_t);
    size_t get_msg_count();
    bool is_phase_iter();
private :
    void receive_phrase();
    void receive_file();
    void save_phrase(const std::string &msg);
    void save_filename(const std::string &msg);

    int sock = -1;
    uint16_t port;
    int epollfd;
    fd_set fds;
    int maxfd;
    sockaddr_in sock_in;
    socklen_t si_len;

    std::vector<std::string> history;

    const std::string name = "receiver";
};

#endif

