#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <ctime>
#include <string>
#include <stdexcept>

#include <stdlib.h>
#include <netdb.h>
#include <sys/epoll.h>

#include "common.h"
#include "tcp_socket.h"
#include "receiver.h"
#include "little.h"

receiver::receiver(const uint16_t tcp_port)
    : port(tcp_port)
{
    struct addrinfo *servinfo;
    sock = tcp_socket(servinfo, port, name);
    if (sock < 0)
    {
        die("receiver::receiver : socket ");
    }
    if (bind(sock, servinfo->ai_addr, servinfo->ai_addrlen) < 0)
    {
        die("receiver::receiver : bind ");
    }
    if (listen(sock, 10) < 0)
    {
        die("receiver::receiver : listen");
    }
    freeaddrinfo(servinfo);

    epollfd = epoll_create1(0);
    if (epollfd < 0)
    {
        die("receiver : epoll_create1");
    }
    epoll_event ev;
    ev.events = EPOLLIN | EPOLLOUT | EPOLLERR;
    ev.data.fd = sock;
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev) == -1)
    {
        die("receiver : epoll_ctl add sock");
    }
}

receiver::~receiver()
{
    close(sock);
    close(epollfd);
}

void receiver::cycle()
{
    const size_t MAX_EVENTS = 20;
    epoll_event events[MAX_EVENTS];
    int cnt = epoll_wait(epollfd, events, MAX_EVENTS, 1);
    if (cnt < 0)
    {
        die("receiver::cycle : epoll_wait");
    }
    for (int i = 0; i < cnt; ++i)
    {
        if (events[i].data.fd == sock)
        {
            if (is_phase_iter())
            {
                receive_phrase();
            } else
            {
                receive_file();
            }
        }
    }
}

bool receiver::is_phase_iter()
{
    return (history.size() % 2) == 0;
}

void receiver::receive_phrase()
{
    try
    {
        little_phrase lr(sock);
        std::string msg = lr.receive_message();
        save_phrase(msg);
    }
    catch (std::runtime_error &e)
    {
        perror("receiver::receive_phrase");
        std::cerr << e.what() << std::endl;
    }
}

void receiver::receive_file()
{
    try
    {
        little_file lr(sock);
        std::string filename = lr.receive_file();
        save_filename(filename);
    }
    catch (std::runtime_error &e)
    {
        perror("receiver::receive_phrase");
        std::cerr << e.what() << std::endl;
    }
}

void receiver::save_filename(const std::string &fn)
{
    history.push_back(fn);
}

void receiver::save_phrase(const std::string &msg)
{
    history.push_back(msg);
}

std::string receiver::get_phrase(size_t iter)
{
    std::string res = history.at(iter);
    return res;
}

size_t receiver::get_msg_count()
{
    return history.size();
}

