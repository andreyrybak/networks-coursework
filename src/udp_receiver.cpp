#include <iostream>

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <string>

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>

/////////////////
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <sys/epoll.h>

#include "ports.h"
#include "ma.h"
#include "prep_msg.h" 
#include "game_state.h"
#include "common.h"

#include "udp.h"
#include "udp_receiver.h"

prep_receiver::prep_receiver(const std::string &interface)
{
    make_udp_socket(sock, sock_in, PREP_PORT, "prep_receiver", true);
    set_timeout(sock, 0, 10000, name);
    si_len = sizeof(sock_in);
}

prep_receiver::~prep_receiver()
{
}

void prep_receiver::operator()()
{
    /// start
    while (game_state::i().need_broadcast())
    {
        cycle();
        sleep(1);
    }
    std::cerr << "prep_receiver ended" << std::endl;
    std::cerr << "prep_receiver ~ CLOSE " << sock << std::endl;
    close(sock);
}

void prep_receiver::cycle()
{
    std::cerr << "prep_receiver cycle" << std::endl;
    fd_set read_fds;
    FD_ZERO(&read_fds);
    FD_SET(sock, &read_fds);
    maxfd = sock;
    timeval tv = {0, 100000};
    int nready = select(maxfd + 1, &read_fds, NULL, NULL, &tv);
    std::cerr << "prep_receiver cycle : nready = " << nready << std::endl;
    if (nready == -1)
    {
        dontdie("prep_receiver::cycle : select");
    }
    for(int i = 0; i <= maxfd && nready > 0; i++)
    {
        if (FD_ISSET(i, &read_fds))
        {
            nready--;
            if (i == sock)
            {
                receive_pm();
            }
        }
    }
}

void prep_receiver::receive_pm()
{
    packed_prep_msg ppm;
    int msg_len = recvfrom(sock, &ppm, sizeof(ppm), 0,
            (struct sockaddr *) &sock_in, &si_len);
    if (msg_len < 0)
    {
        dontdie("prep_recv :: here");
        sleep(1);
    } else
    {
        std::string login(ppm.login);
        std::cout << "Login " << login << " count = " << ppm.count << std::endl;
        uint32_t ip = sock_in.sin_addr.s_addr;
        game_state::i().new_broadcast(ppm, ip);
    }
}

/// CHECK
check_receiver::check_receiver()
{
    make_udp_socket(sock, sock_in, CHECK_PORT, "check_receiver", true);
    set_timeout(sock, 0, 10000, name);
    si_len = sizeof(sock_in);
}

void check_receiver::operator()()
{
    std::cerr << "check_receiver " << game_state::i().get_id() << std::endl;

    while (game_state::i().need_check())
    {
        std::cerr << "check_receiver WHILE" << std::endl;
        cycle();
        sleep(1);
    }
    std::cerr << "check_receiver ended" << std::endl;
    std::cerr << "check_receiver ~ CLOSE " << sock << std::endl;
    close(sock);
}

void check_receiver::cycle()
{
    fd_set read_fds;
    FD_ZERO(&read_fds);
    FD_SET(sock, &read_fds);
    maxfd = sock;
    timeval tv = {0, 100000};
    int nready = select(maxfd + 1, &read_fds, NULL, NULL, &tv);
    if (nready == -1)
    {
        dontdie("check_receiver::cycle : select");
    }
    for(int i = 0; i <= maxfd && nready > 0; i++)
    {
        if (FD_ISSET(i, &read_fds))
        {
            nready--;
            if (i == sock)
            {
                receive_cm();
            }
        }
    }
}

void check_receiver::receive_cm()
{
    packed_check_msg pcm;
    int msg_len = recvfrom(sock, &pcm, sizeof(pcm), 0,
            (struct sockaddr *) &sock_in, &si_len);
    uint32_t ip = sock_in.sin_addr.s_addr;
    if (msg_len < 0)
    {
        dontdie("check_recv");
        sleep(1);
    } else
    {
        std::cerr << "check received " << std::endl;
        game_state::i().recv_check(pcm, ip);
    }
}

check_receiver::~check_receiver()
{
}

