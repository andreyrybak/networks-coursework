#include <cstdlib>
#include <ctime>
#include <sys/types.h>
#include <unistd.h>
#include <cstring>

#include <stdexcept>
#include <iostream>
#include <utility>
#include <string>
#include <sys/epoll.h>

#include <vector>

#include "ports.h"
#include "tcp_socket.h"
#include "game_state.h"
#include "common.h"
#include "little.h"
#include "tcp_receiver.h" 

ips_receiver::ips_receiver()
{
    {
        sock = tcp_socket(servinfo, IPS_PORT, name);
        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 200000;

        if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
        {
            throw std::runtime_error("ips_receiver : setsockopt : sock");
        }


        if (bind(sock, servinfo->ai_addr, servinfo->ai_addrlen) < 0)
        {
            die("ips_receiver : bind");
        }
        if (listen(sock, 10) < 0)
        {
            die("ips_receiver : listen");
        }
        freeaddrinfo(servinfo);
    }
    {
        epollfd = epoll_create1(0);
        if (epollfd < 0)
        {
            die("ips_receiver : epoll_create1");
        }
        epoll_event ev;
        ev.events = EPOLLIN | EPOLLOUT | EPOLLERR;
        ev.data.fd = sock;
        if (epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev) == -1)
        {
            die("ips_receiver : epoll_ctl add sock");
        }
    }
}

void ips_receiver::operator()(const std::vector<uint32_t> &ips)
{
    bool bad = false;
    int size = game_state::i().get_count();
    while (game_state::i().need_check() && (size > 0))
    {
        std::cerr << "here" << std::endl;
        const size_t MAX_EVENTS = 20;
        epoll_event events[MAX_EVENTS];
        int cnt = epoll_wait(epollfd, events, MAX_EVENTS, 1);
        if (cnt < 0)
        {
            die("ips_receiver : epoll_wait");
        }
        
        for (int i = 0; i < cnt; ++i)
        {
            if (events[i].data.fd == sock)
            {
                try
                {
                    little_ips lr(sock);
                    uint32_t ip;
                    auto ans = lr.receive_message(ip);
                    std::cerr << "ips.size() == " << ips.size() << std::endl;
                    std::cerr << "ans.size() == " << ans.size() << std::endl;
                    if (ans.size() != ips.size())
                    {
                        game_state::i().recv_bad(ip);
                        bad = true;
                        break;
                    } else
                    {
                        for (size_t i = 0; i < ans.size(); ++i)
                        {
                            if (ans[i] != ips[i])
                            {
                                game_state::i().recv_bad(ip);
                                bad = true;
                                break;
                            }
                        }
                    }
                    --size;
                }
                catch (std::runtime_error &e)
                {
                    perror("ips_receiver::receive_message");
                    std::cerr << e.what() << std::endl;
                }
            }
        }
        sleep(1);
    }
    close(sock);
    close(epollfd);
    if (!bad)
    {
        game_state::i().all_good();
    }
}

