#ifndef PREP_MSG_H
#define PREP_MSG_H
#include "ma.h"

typedef unsigned char sha_hash[20]; // == 20
const size_t LOGIN_LEN = 30;
typedef char login_type[LOGIN_LEN];

struct __attribute__ ((packed))
    packed_prep_msg
{
    _mac_addr_t ma;
    sha_hash hash;
    int count;
    login_type login;
};

enum check_status
{
    BEFORE = 0x0,
    CHECKING = 0xAAAAAAAA,
    ALL_SENT = 0x33333333,
    BAD = 0xFFFFFFFF,
    GOOD = 0x5555555
};


struct __attribute__ ((packed))
    packed_check_msg
{
    check_status cs;
};

#endif

