#include <cstring>
#include <iostream>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "common.h"

#include "udp.h"

void make_udp_socket(int &fd, struct sockaddr_in &sock, uint16_t port, std::string caller, bool is_receiver)
{
    fd = socket(PF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
    {
        die("socket");
    } else
    {
        std::cerr << caller << " SOCKET " << fd << std::endl;
    }
    if (is_receiver)
    {
        socklen_t si_len = sizeof(sock);
        memset(&sock, 0, si_len);
        sock.sin_family = AF_INET;
        sock.sin_port = htons(port);
        sock.sin_addr.s_addr = htonl(INADDR_ANY);
        int status = bind(fd, (struct sockaddr *) &sock, si_len);
        if (status < 0)
        {
            die((caller + " bind : FAIL ").c_str());
        } else
        {
            std::cerr << caller << " bind : OK " << std::endl;
        }
    }
}

void bind_device(int sock, const std::string &interface_name, const std::string &caller)
{
    ifreq interface;
    memset(&interface, 0, sizeof(interface));
    strncpy(interface.ifr_ifrn.ifrn_name,
            interface_name.c_str(), IFNAMSIZ);
    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, &interface, sizeof(interface)) < 0)
    {
        dontdie(caller + " : setsockopt : DEVICE : FAIL");
    } else
    {
        std::cerr << caller << " : setsockopt : DEVICE : OK " << interface_name << std::endl;
    }
}

void set_broadcast(int sock, const std::string &caller)
{
    int yes = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int)) < 0)
    {
        die(caller + " : setsockopt : BROADCAST : FAIL");
    } else
    {
        std::cerr << caller << " : setsockopt : BROADCAST : OK" << std::endl;
    }
}

void set_timeout(int sock, const time_t sec, const suseconds_t usec, const std::string &caller)
{
    timeval tv;
    tv.tv_sec = sec;
    tv.tv_usec = usec;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        die(caller + " : setsockopt : timeout : FAIL ");
    } else 
    {
        std::cerr << caller << " : setsockopt : timeout : OK " << std::endl;
    }
}

