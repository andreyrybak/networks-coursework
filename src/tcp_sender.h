#ifndef TCP_SENDER_H
#define TCP_SENDER_H

#include <vector>
#include <string>

struct ips_sender
{
    void operator()(const std::vector<uint32_t> &);
private :
    const std::string name = "ips_sender";
};

#endif
