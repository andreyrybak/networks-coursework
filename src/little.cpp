#include <stdexcept>
#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <netinet/in.h>

#include "common.h"
#include "little.h"

little_ips::little_ips(int sock)
    : buffer(NULL)
{
    conn_sock = accept(sock, NULL, NULL);
    if (conn_sock < 0)
    {
        throw std::runtime_error("little_ips::little_ips : accept");
    } else
    {
        std::cerr << "ACCEPT : " << conn_sock << std::endl;
    }

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 200000;
    if (setsockopt(conn_sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("little_ips::little_ips : setsockopt : conn_sock");
    }
}

little_ips::~little_ips()
{
    std::cerr << "little_ips CLOSE " << conn_sock << std::endl;
    delete[] buffer;
    close(conn_sock);
}

std::vector<uint32_t> little_ips::receive_message(uint32_t &ip)
{
    int cnt = 0;
    size_t h;
    sockaddr_in sock_in;
    socklen_t si_len = sizeof(sock_in);
    cnt = recvfrom(conn_sock, &h, sizeof(h), 0, (struct sockaddr *) &sock_in, &si_len);
    std::cerr << "little_ips :: recvfrom " << cnt << std::endl;
    ip = sock_in.sin_addr.s_addr;
    if (cnt < 0)
    {
        throw std::runtime_error("little_ips::receive_message : recvfrom header");
    }
    std::vector<uint32_t> res(h, 0);
    size_t buflen = h * sizeof(uint32_t);
    buffer = new char[buflen];
    cnt = recvfrom(conn_sock, buffer, buflen, 0, NULL, NULL);
    if (cnt < 0)
    {
        throw std::runtime_error("little_ips::receive_message : recvfrom message");
    }
    std::cerr << "little_ips :: recvfrom2 " << cnt << std::endl;
    for (size_t i = 0; i < h; ++i)
    {
        res[i] = *((uint32_t *) (buffer + i * sizeof(uint32_t)));
    }
    return res;
}

little_phrase::little_phrase(int sock)
    : buffer(NULL)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 200000;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("little_phrase::little_phrase : setsockopt : sock");
    }
    conn_sock = accept(sock, NULL, NULL);
    if (conn_sock < 0)
    {
        throw std::runtime_error("little_phrase::little_phrase : accept");
    }
    if (setsockopt(conn_sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("little_phrase::little_phrase : setsockopt : conn_sock");
    }
}

little_phrase::~little_phrase()
{
    delete[] buffer;
    close(conn_sock);
}

std::string little_phrase::receive_message()
{
    int cnt = 0;
    size_t h;
    cnt = recvfrom(conn_sock, &h, sizeof(h), 0, NULL, NULL);
    if (cnt < 0)
    {
        throw std::runtime_error("little_phrase::receive_message : recvfrom h");
    }
    uint32_t len = h;
    buffer = new char[len + 1];
    memset(buffer, 0, len + 1);
    cnt = recvfrom(conn_sock, buffer, len, 0, NULL, NULL);
    if (cnt < 0)
    {
        throw std::runtime_error("little_phrase::receive_message : recvfrom message");
    }
    buffer[len] = 0;
    std::string msg(buffer);
    return msg;
}

////////////////////////////
little_file::little_file(int sock)
    : buffer(NULL), filebuf(NULL)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 200000;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("little_file::little_file : setsockopt : sock");
    }
    conn_sock = accept(sock, NULL, NULL);
    if (conn_sock < 0)
    {
        throw std::runtime_error("little_file::little_file : accept");
    }
    if (setsockopt(conn_sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("little_file::little_file : setsockopt : conn_sock");
    }
}

little_file::~little_file()
{
    delete[] buffer;
    delete[] filebuf;
    close(conn_sock);
}

namespace
{
    std::string generate_filename(const std::string fn)
    {
        static const size_t len = 8;
        size_t pos = fn.find_last_of('.');
        std::string ext = fn.substr(pos);
        std::string gen;
        for (size_t i = 0; i < len; ++i)
        {
            char c = rand() % ('Z' - 'A') + 'a';
            gen.push_back(c);
        }
        gen.append(ext);
        return gen;
    }
}

std::string little_file::receive_file()
{
    int cnt = 0;
    size_t h[2];
    cnt = recvfrom(conn_sock, h, sizeof(h), 0, NULL, NULL);
    if (cnt < 0)
    {
        throw std::runtime_error("little_file::receive_file : recvfrom h");
    }
    size_t len = h[0];
    size_t filelen = h[1];
    std::cerr << "little_file : receive_file : filelen = " << filelen << std::endl;
    buffer = new char[len + 1]; memset(buffer, 0, len + 1);
    cnt = recvfrom(conn_sock, buffer, len, 0, NULL, NULL);
    buffer[len] = 0;
    if (cnt < 0)
    {
        throw std::runtime_error("little_file::receive_file : recvfrom filename");
    }
    std::string filename(buffer);
    std::cerr << "little_file : receive_file : filename = " << filename << std::endl;
    filebuf = new char[filelen]; memset(filebuf, 0, filelen);
    cnt = 0;
    int received = 0;
    while ((cnt = recvfrom(conn_sock, filebuf + received, filelen - received, 0, NULL, NULL)) > 0)
    {
        std::cerr << "little_file : receive_file : recvfrom filebuf cnt = " << cnt << std::endl;
        received += cnt;
    }
    if (cnt < 0)
    {
        throw std::runtime_error("little_file::receive_file : recvfrom FILE");
    }
    try
    {
        std::string newfilename = generate_filename(filename);
        std::cerr << "little_file : saving file " << newfilename << std::endl;
        std::ofstream output(newfilename, std::ios::binary); 
        output.write(filebuf, filelen);
        output.close();
        return newfilename;
    } catch (std::exception &e)
    {
        die(e.what());
    }
}

