#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <unordered_map>
#include <string>
#include <vector>
#include <set>

#include "prep_msg.h"

struct player_status
{
    int count;
    std::string login;
    uint32_t ip;
};


struct game_state
{
    static game_state& i();
    int get_count() const;
    long long get_id() const;
    bool need_broadcast() const;
    void new_broadcast(const packed_prep_msg &, uint32_t);
    const time_t TIME_READY = 5;

    void start_check();
    bool need_check();
    check_status get_check_status() const;
    void recv_check(const packed_check_msg &, uint32_t);
    void recv_bad(uint32_t);
    void all_good();
    void all_sent();
    std::vector<uint32_t> ips();
    std::vector<player_status> get_order();
    uint32_t my_ip(const std::string &);
private :
    game_state();
    time_t last_change_time;
    int r;
    std::unordered_map<long long, player_status> players;

    check_status cs;

    std::set<uint32_t> ipsset;
};

#endif

