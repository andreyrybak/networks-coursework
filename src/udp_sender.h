#ifndef UDP_SENDER
#define UDP_SENDER

#include <string>
struct prep_sender
{
    prep_sender(const std::string &);
    ~prep_sender();
    void operator()(const std::string &,
            const std::string &);
private:
    int sock;
    sockaddr_in aa;
    const std::string name = "prep_sender";
};

struct check_sender
{
    check_sender();
    ~check_sender();
    void operator()();
private:
    int sock;
    sockaddr_in aa;
    const std::string name = "check_sender";
};

#endif

