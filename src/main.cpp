#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include <thread>

#include <string>

#include <unistd.h>

#include "game_state.h"
#include "udp_receiver.h"
#include "udp_sender.h"
#include "tcp_receiver.h"
#include "tcp_sender.h"
#include "sender.h"
#include "receiver.h"
#include "ports.h"

using std::string;

void read_cfg(string &e, string &v)
{
    using std::getline;
    std::ifstream cfg("pic.cfg");
    if (cfg.good())
    {
        getline(cfg, e);
        getline(cfg, v);
    }
}

void external_call(const string &cmd)
{
    int res = system(cmd.c_str());
    if (res < 0)
    {
        std::cerr << "external_call " << cmd << " error " << std::endl;
    }
}

const bool DEBUG = false;
int main(int argc, char *argv[])
{
    using std::cout;
    using std::cin;
    using std::cerr;
    using std::endl;
    string EDITOR = "pinta";
    string VIEWER = "eog";
    string interface = "wlan0";
    read_cfg(EDITOR, VIEWER);
    if (argc == 2)
    {
        interface = std::string(argv[1]);
    }

    cerr << "main id : " << game_state::i().get_id() << endl;
    string login;
    cout << "Enter login : " << endl;
    if (DEBUG)
    {
        login = "test";
    } else
    {
        getline(cin, login);
    }

    do
    {
        prep_receiver PR(interface);
        prep_sender PS(interface);
        std::thread t_ps(PS, login, interface);
        std::thread t_pr(PR);
        t_ps.join();
        t_pr.join();
        game_state::i().start_check();
//        check_receiver CR;
//        check_sender CS;
//        std::thread t_cr(CR);
//        std::thread t_cs(CS);
        ips_receiver IR;
        ips_sender IS;
        std::thread t_ir(IR, game_state::i().ips());
        std::thread t_is(IS, game_state::i().ips());
        while (game_state::i().need_check())
        {
            sleep(1);
        }
//        t_cr.join();
//        t_cs.join();
        t_is.join();
        t_ir.join();
    } while (game_state::i().get_check_status() != GOOD);
    
    auto order = game_state::i().get_order();
    cerr << "order.size() = " << order.size() << endl;
    uint32_t my_ip = game_state::i().my_ip(interface);
    size_t my_pos = 0;
    for (size_t i = 0; i < order.size(); ++i)
    {
        if (order[i].ip == my_ip)
        {
            my_pos = i;
            break;
        }
    }
    size_t my_next = my_pos + 1;
    if (my_next == order.size())
    {
        my_next = 0;
    }
    int next_ip = order[my_next].ip;
    receiver R(PHRASE_PORT);
    cout.flush();
    for (size_t iter = 0; iter < 2 * order.size(); ++iter)
    {
        R.cycle();
        cout << "Cycle #" << iter << endl;
        sender S(PHRASE_PORT);
        if (iter % 2 == 0)
        {
            cout << "Phrase " << endl;
            string phrase;
            getline(cin, phrase);
            S.send_phrase(next_ip, phrase, login);
        } else
        {
            external_call(EDITOR);
            cout << "Filename " << endl;
            string filename;
            getline(cin, filename);
            S.send_file(next_ip, filename, login);
        }
        while (R.get_msg_count() <= iter)
        {
            R.cycle();
        }
        std::string inbox = R.get_phrase(iter);
        if (R.is_phase_iter())
        {
            external_call((VIEWER + " " + inbox).c_str());
        } else
        {
            cout << "Draw picture of : " << inbox << endl;
        }
    }
//    while(true)
//    {
//        
//    }
    //external_call(EDITOR);
    //string filename;
    //std::getline(std::cin, filename);
    //external_call(VIEWER + " " + filename);
}

