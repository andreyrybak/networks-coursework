#include <cstdint>
#include <cstring>

#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <stdexcept>

#include "common.h"
#include "tcp_socket.h"

#include "sender.h"

sender::sender(uint16_t port)
    : port(port)
{
    struct addrinfo *servinfo;
    sock = tcp_socket(servinfo, port, name);
    freeaddrinfo(servinfo);
}

sender::~sender()
{
    close(sock);
}

void sender::send_phrase(const int ip, const std::string &text, const std::string &login)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 200000;
    if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("sender::send_message : setsockopt : timeval"); }
    sockaddr_in addr = {AF_INET, htons(port), {0}, {0,0,0,0,0,0,0,0}};
    addr.sin_addr.s_addr = ip;
    if (connect(sock, (sockaddr *) &addr, sizeof(addr)) < 0)
    {
        throw std::runtime_error("sender::send_message : connect");
    }
    size_t h;
    std::string msg = login + " " + text;
    h = msg.length();
    size_t buflen = msg.length() + sizeof(h);
    char *buffer = new char[buflen];
    memcpy(buffer, &h, sizeof(h));
    memcpy(buffer + sizeof(h), msg.c_str(), msg.length());
    send_sure(sock, buffer, buflen, "sender msg ");
}

namespace
{
    size_t get_filesize(const std::string &filename)
    {
        std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
        in.seekg(0, std::ifstream::end);
        return in.tellg(); 
    }
}

void sender::send_file(const int ip, const std::string &filename,
        const std::string &login)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 200000;
    if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
    {
        throw std::runtime_error("sender::send_file : setsockopt : timeval");
    }
    sockaddr_in addr = {AF_INET, htons(port), {0}, {0,0,0,0,0,0,0,0}};
    addr.sin_addr.s_addr = ip;
    if (connect(sock, (sockaddr *) &addr, sizeof(addr)) < 0)
    {
        throw std::runtime_error("sender::send_file : connect");
    }
    size_t h[2];
    h[0] = filename.length();
    size_t filesize = get_filesize(filename);
    char *filebuf = new char[filesize];
    h[1] = filesize;
    size_t buflen = filename.length() + sizeof(h);
    char *buffer = new char[buflen];
    memcpy(buffer, h, sizeof(h));
    memcpy(buffer + sizeof(h), filename.c_str(), filename.length());
    std::cerr << name << " send_file : filesize = " << filesize << std::endl;
    send_sure(sock, buffer, buflen, "sender filename ");
    try
    {
        std::ifstream input(filename, std::ios::binary);
        input.read(filebuf, filesize);
        input.close();
    } catch (std::exception &e)
    {
        die(("sender " + filename + e.what()).c_str());
    }
    send_sure(sock, filebuf, filesize, "sender file ");
    delete[] buffer;
    delete[] filebuf;
}

