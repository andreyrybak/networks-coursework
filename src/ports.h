#ifndef PORTS_H
#define PORTS_H

#include <cstdint>

const uint16_t PREP_PORT = 1337;
const uint16_t CHECK_PORT = 1701;
const uint16_t IPS_PORT = 1852;
const uint16_t PHRASE_PORT = 1025;

#endif
