#include <cstdint>
#include <cstring>

#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <iostream>
#include <stdexcept>

#include "ports.h"
#include "tcp_sender.h"
#include "tcp_socket.h"
#include "game_state.h"

void ips_sender::operator()(const std::vector<uint32_t> &ips)
{
    for (auto ip : ips)
    {
        struct addrinfo *servinfo;
        int sock = tcp_socket(servinfo, IPS_PORT, name);
        freeaddrinfo(servinfo);

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 200000;
        if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
        {
            throw std::runtime_error("ips_sender : setsockopt : timeval");
        }
        sockaddr_in addr = {AF_INET, htons(IPS_PORT), {0}, {0,0,0,0,0,0,0,0}};
        addr.sin_addr.s_addr = ip;
        while (connect(sock, (sockaddr *) &addr, sizeof(addr)) < 0)
        {
            perror("ips_sender perror ");
            // throw std::runtime_error("ips_sender : connect");
            sleep(1);
            std::cerr << "ips_sender : sleep OK" << std::endl;
        }
        size_t size = ips.size();
        size_t buflen = sizeof(size_t) + sizeof(uint32_t) * size;
        char *buffer = new char[buflen];
        memcpy(buffer, &size, sizeof(size));
        memcpy(buffer + sizeof(size), ips.data(), buflen - sizeof(size));
        send_sure(sock, buffer, buflen, name + " ips ");
        close(sock);
    }
    game_state::i().all_sent();
}


