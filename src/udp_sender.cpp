#include <ostream>
#include <iostream>

#include <netinet/in.h>
#include <unistd.h>

#include <boost/uuid/sha1.hpp>
#include <boost/array.hpp>

#include <string>
#include <cstring>
#include <algorithm>

#include "common.h"
#include "ports.h"
#include "ma.h"
#include "prep_msg.h" 
#include "game_state.h"

#include "udp.h"
#include "udp_sender.h"

using std::string;
namespace
{
 
    void display(char* hash, std::ostream &out)
    {
        out << std::hex;
        for(int i = 0; i < 20; ++i)
        {
            out << ((hash[i] & 0x000000F0) >> 4) 
                <<  (hash[i] & 0x0000000F);
        } 
    }
     
    void calc_sha(sha_hash &h, string a)
    {
        boost::uuids::detail::sha1 s;
        s.process_bytes(a.c_str(), a.size());
        unsigned int digest[5];
        s.get_digest(digest);
        for(int i = 0; i < 5; ++i)
        {
            const char* tmp = reinterpret_cast<char*>(digest);
            h[i*4] = tmp[i*4+3];
            h[i*4+1] = tmp[i*4+2];
            h[i*4+2] = tmp[i*4+1];
            h[i*4+3] = tmp[i*4];
        }
    }
}

prep_sender::prep_sender(const std::string &interface_name)
{
    std::cerr << "prep_sender::prep_sender" << std::endl;
    make_udp_socket(sock, aa, PREP_PORT, "prep_sender");
    aa.sin_family = AF_INET;
    aa.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    aa.sin_port = htons(PREP_PORT);
    set_broadcast(sock, name);
    // bind_device(sock, interface_name, name);
}

void prep_sender::operator()(const string &login, const string &interface)
{
    using std::cerr;
    using std::endl;
    packed_prep_msg ppm;
    {
        mac_addr_t mac;
        get_mac(mac, interface);
        string m = mac2string(mac);
        string h = login + m;
        mov(ppm.ma, mac.ma);
        calc_sha(ppm.hash, h);
        strncpy(ppm.login, login.c_str(),
                std::min(LOGIN_LEN - 1, login.length()));
        ppm.login[LOGIN_LEN - 1] = 0;
        cerr << interface << " mac : " << m << endl;
    }

    const unsigned int UDP_INTERVAL = 1;
    cerr << "prep_sender ID " << game_state::i().get_id() << endl;
    while(game_state::i().need_broadcast())
    {
        ppm.count = game_state::i().get_count();
        size_t msg_size = sizeof(ppm);
        if (sendto(sock, &ppm, msg_size, 0,
                    (struct sockaddr *) &aa, sizeof(aa)) < 0)
        {
            dontdie("FAIL::prep_sender : sendto");
        } else
        {
            std::cerr << "prep_sender : sendto OK " << endl;
        }
        sleep(UDP_INTERVAL);
    }
    cerr << "prep_sender ended" << endl;
    std::cerr << "prep_sender ~ CLOSE " << sock << std::endl;
    close(sock);
}

prep_sender::~prep_sender()
{
}
////////// CHECK
check_sender::check_sender()
{
    make_udp_socket(sock, aa, CHECK_PORT, "check_sender");
    aa.sin_family = AF_INET;
    aa.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    aa.sin_port = htons(CHECK_PORT);
    set_broadcast(sock, name);
}

check_sender::~check_sender()
{
}

void check_sender::operator()()
{
    using std::cerr;
    using std::endl;
    packed_check_msg pcm;
    const unsigned int UDP_INTERVAL = 1;
    cerr << "check_sender " << game_state::i().get_id() << endl;
    while(game_state::i().need_check())
    {
        pcm.cs = game_state::i().get_check_status();
        size_t msg_size = sizeof(pcm);
        if (sendto(sock, &pcm, msg_size, 0,
                    (struct sockaddr *) &aa, sizeof(aa)) < 0)
        {
            dontdie("check_sender : sendto : FAIL ");
        } else
        {
            cerr << ("check_sender : sendto : OK") << endl;
        }
        sleep(UDP_INTERVAL);
    }
    cerr << "check_sender ended" << endl;
    std::cerr << "check_sender ~ CLOSE " << sock << std::endl;
    close(sock);
}

