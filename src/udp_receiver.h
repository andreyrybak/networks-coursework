#ifndef UDP_RECEIVER
#define UDP_RECEIVER

#include <string>
#include <netinet/in.h>

struct prep_receiver
{
    prep_receiver(const std::string &);
    ~prep_receiver();
    void operator()();

private:
    void cycle();

    void receive_pm();
    int sock;
    sockaddr_in sock_in;
    socklen_t si_len;

    fd_set fds;
    int maxfd;
    const std::string name = "prep_receiver";
};

struct check_receiver
{
    check_receiver();
    ~check_receiver();
    void operator()();
private:
    void cycle();
    void receive_cm();
    int sock;
    sockaddr_in sock_in;
    socklen_t si_len;

    fd_set fds;
    int maxfd;
    const std::string name = "check_receiver";
};

#endif
