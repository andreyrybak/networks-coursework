#include <iostream>
#include <sys/socket.h>
#include <netdb.h>
#include <cstdio>

#include "common.h"

#include "tcp_socket.h"

int tcp_socket(addrinfo * &servinfo, uint16_t port, const std::string &caller)
{
    int status;
    struct addrinfo hints = {AI_PASSIVE, AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, 0, 0};
    char service[10];
    sprintf(service, "%d", port);
    if ((status = getaddrinfo(NULL, service, &hints, &servinfo)) != 0)
    {
        die(caller + " tcp_socket : getaddrinfo");
    }
    int sock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sock < 0)
    {
        die(caller + " tcp_socket : socket");
    } else
    {
        std::cerr << caller << " tcp_socket : sock = " << sock << std::endl;
    }
    int yes = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
    {
        die(caller + " tcp_socket : setsockopt : reuse ");
    } else
    {
        std::cerr << caller << " tcp_socket : reuse OK " << std::endl;
    }
    return sock;
}

void send_sure(int sock, char *buf, size_t buflen, const std::string &caller)
{
    int cnt = 0;
    int send_cnt = 0;
    while ((cnt = send(sock, buf + send_cnt, buflen - send_cnt, 0)) > 0)
    {
        send_cnt += cnt;
        std::cerr << caller << " : send_sure cnt = " << cnt << std::endl;
    }
    if (cnt < 0)
    {
        die(caller + " : send_sure send FAIL ");
    }
    std::cerr << caller << " : send_sure OK " << std::endl;
}

