#include <ctime>
#include <iostream>
#include <cstdlib>
#include <iterator>
#include <vector>
#include <set>
#include <algorithm>

#include "game_state.h"
#include "prep_msg.h"

game_state& game_state::i()
{
    static game_state res;
    return res;
}

game_state::game_state()
    : players(), cs(BEFORE)
{
    last_change_time = time(NULL);
    srand(time(NULL));
    r = rand();
}

int game_state::get_count() const
{
    return players.size();
}

check_status game_state::get_check_status() const
{
    return cs;
}

void game_state::new_broadcast(const packed_prep_msg &msg, uint32_t ip)
{
    std::cerr << "new_broadcast ip = " << ip << std::endl;
    mac_addr_t m;
    mov(m.ma, msg.ma);
    if (players.count(m.id) == 0)
    {
        std::string login(msg.login);
        player_status p = {msg.count, login, ip};
        players.emplace(m.id, p);
        if (msg.count != get_count())
        {
            last_change_time = time(NULL);
        }
    }
}

bool game_state::need_broadcast() const
{
    time_t ct = time(NULL);
    long long dif = ct - last_change_time;
    std::cerr << "game_state time to game " << TIME_READY - dif << std::endl;
    if ((dif > TIME_READY) && (players.size() > 0))
    {
        return false;
    } else
    {
        return true;
    }
}

bool game_state::need_check()
{
    std::cerr << "ips size = " << ipsset.size() <<
        " get_count() = " << get_count() << std::endl;
    if (ipsset.size() == get_count())
    {
        cs = GOOD;
    }
    std::string s;
    switch (cs)
    {
        case BEFORE:
            s = "before";
            break;
        case BAD:
            s = "bad";
            break;
        case GOOD:
            s = "good";
            break;
        case ALL_SENT:
            s = "all sent";
            break;
        case CHECKING:
            s = "checing";
            break;
    }
    std::cerr << "need_check : " << s << std::endl;
    return (cs != BEFORE) && (cs != GOOD);
}

long long game_state::get_id() const
{
    return (long long) (this) + r;
}

void game_state::start_check()
{
    cs = CHECKING;
    
}

void game_state::recv_check(const packed_check_msg &pcm, uint32_t ip)
{
    static std::set<uint32_t> as_ips;
    ipsset.insert(ip);
    switch (pcm.cs)
    {
        case ALL_SENT:
            as_ips.insert(ip);
            break;
        case BAD:
            cs = BAD;
            break;
        default:
            break;
    }
    if (ipsset.size() == get_count())
    {
        cs = GOOD;
    }
}

void game_state::recv_bad(uint32_t ip)
{
    std::cerr << "game_state : recv_bad " << ip << std::cerr;
    cs = BAD;
}

void game_state::all_good()
{
    cs = GOOD;
}

std::vector<uint32_t> game_state::ips()
{
    std::vector<uint32_t> res;
    for (auto it = players.begin(); it != players.end(); ++it)
    {
        player_status p(it->second);
        res.push_back(p.ip);
    }
    std::sort(res.begin(), res.end());
    return res;
}

void game_state::all_sent()
{
    std::cerr << "game_state::all_sent" << std::endl;
    if (cs == BAD)
    {
        cs = BEFORE;
    } else
    {
        cs = ALL_SENT;
    }
}

uint32_t game_state::my_ip(const std::string &interface)
{
    mac_addr_t my_mac;
    get_mac(my_mac, interface);
    for (auto it = players.begin(); it != players.end(); ++it)
    {
        if (my_mac.id == it->first)
        {
            return it->second.ip;
        }
    }
    return 0;
}

std::vector<player_status> game_state::get_order()
{
    std::vector<long long> macs;
    std::cerr << "get_order : players.size() " << players.size()
        << std::endl;
    for (auto it = players.begin(); it != players.end(); ++it)
    {
        macs.push_back(it->first);
        std::cerr << "mac : " << it->first << std::endl;
    }
    std::sort(macs.begin(), macs.end());
    std::vector<player_status> res;
    for (auto m : macs)
    {
        std::cerr << "mac2 : " << m << std::endl;
        res.push_back(players.at(m));
    }
    std::cerr << std::endl;
    return res;
}

