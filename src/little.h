#ifndef LITTLE_H
#define LITTLE_H
#include <vector>
#include <string>

struct little_ips
{
    little_ips(int);
    ~little_ips();
    std::vector<uint32_t> receive_message(uint32_t &);
private:
    int conn_sock;
    char *buffer;
};

struct little_phrase
{
    little_phrase(int);
    ~little_phrase();
    std::string receive_message();
private:
    int conn_sock;
    char *buffer;
};

struct little_file
{
    little_file(int);
    ~little_file();
    std::string receive_file();
private:
    int conn_sock;
    char *buffer;
    char *filebuf;
};


#endif
