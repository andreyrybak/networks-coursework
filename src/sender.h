#ifndef SENDER_H
#define SENDER_H

#include <string>

struct sender
{
    sender(const uint16_t);
    ~sender();
    void send_phrase(const int ip, const std::string &, const std::string &);
    void send_file  (const int ip,   const std::string &, const std::string &);
private:
    int sock;
    uint16_t port;
    const std::string name = "sender";
};

#endif
